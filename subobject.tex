\documentclass{article}

\usepackage[plain]{fullpage}
\usepackage{tikz}
%%%<
% \usepackage[active,tightpage]{preview}
% \PreviewEnvironment{tikzpicture}
% \setlength\PreviewBorder{5pt}%
%%%>
\usetikzlibrary{fit}                      % fitting shapes to coordinates
\usetikzlibrary{backgrounds}              % drawing the background after the foreground
\usetikzlibrary{matrix}

%\tikzstyle{background}=[rectangle, fill=green!15, inner sep=0mm, rounded corners=2mm]
\tikzstyle{object}=[rectangle, fill=green!10, draw=black, opacity=0.5, inner sep=0mm, rounded corners=2mm]

\def\arr{\{0\rightarrow 1\}}
\def\sets{{\bf Sets}}
\def\terminal{{\bf 1}}
\def\aset#1{\{#1\}}
\def\true{\mbox{true}}
\def\ttt{\mbox{\sf t}}
\def\fff{\mbox{\sf f}}
\def\uuu{\mbox{\sf u}}

\title{Subobject Classifer}

\begin{document}

From {\sl Sheaves in Geometry and Logic\/} by Mac Lane and Moerdijk 1992.

\section{Subobject Classifier}

\subsection{Functor Category}

With arrow category $\arr $, the functor category $\sets^{\arr}$ has

\begin{itemize}
\item Objects are functors ($S$, $X$) from $\arr$ to $\sets$.  Their images in $\sets$ are represented

  \begin{tikzpicture}
  \matrix (m) [matrix of math nodes,row sep=3em,column sep=4em,minimum width=2em]
          {
            |(s0)| S0 \\
            |(s1)| S1 \\
          };
          \path[-stealth]
          (s0) edge node [left] {$Sf$} (s1);
    \begin{pgfonlayer}{background}
        \node [object, fit=(s0) (s1)] {};
    \end{pgfonlayer}
  \end{tikzpicture}

where we use $Sf$ to denote some function for each object.

\item Arrows are natural transformations $\alpha : S \rightarrow X$.  I.e., the following commutes:

\begin{tikzpicture}
  \matrix (m) [matrix of math nodes,row sep=3em,column sep=4em,minimum width=2em]
          {
            |(s0)| S0 & |(x0)| X0 \\
            |(s1)| S1 & |(x1)| X1 \\
          };
          \path[-stealth]
          (s0) edge node [left] {$Sf$} (s1)
          (x0) edge node [left] {$Xf$} (x1)
          (s0) edge node [above] {$\alpha 0$} (x0)
          (s1) edge node [above] {$\alpha 1$} (x1)
          ;
    \begin{pgfonlayer}{background}
        \node [object, fit=(s0) (s1)] {};
        \node [object, fit=(x0) (x1)] {};
    \end{pgfonlayer}
\end{tikzpicture}

\end{itemize}

\subsection{Terminal Object}

The terminal object is $\terminal = \{ ! : \aset{0} \rightarrow \aset{0} \}$ since $\aset{0}$ is terminal in $\sets$.

\subsection{Subset}

$S$ is a subset of $X$ if $S0 \subset X0$, $S1 \subset X1$, and $Sf(S0) \subset S1$.

\begin{tikzpicture}[>=stealth]
  \matrix (m) [matrix of math nodes,row sep=3em,column sep=4em,minimum width=2em]
          {
            |(s0)| S0 &  \\
                      & |(s1)| S1 \\
            |(x0)| X0 & \\
                      & |(x1)| X1 \\
          };
          \path[-stealth]
          (s0) edge node [above] {$Sf$} (s1)
          (x0) edge node [above] {$Xf$} (x1)
          (s0) edge[>->] node [left] {$\alpha 0$} (x0)
          (s1) edge[>->] node [left] {$\alpha 1$} (x1)
          ;
    \begin{pgfonlayer}{background}
        \node [object, fit=(s0) (s1)] {};
        \node [object, fit=(x0) (x1)] {};
    \end{pgfonlayer}
\end{tikzpicture}


\subsection{Example}

Let $S0 = \aset{a,b}$, $S1 = \aset{a,b,c}$, $X0 = \aset{a,b,c,d}$, and $X1 = \aset{a,b,c,d,e}$ with inclusions
\[ \alpha 0 = \{ a \mapsto a, b \mapsto b \} \]
\[ \alpha 1 = \{ a \mapsto a, b \mapsto b, c \mapsto c \} \]
For $S0$ and $X0$, the subobject classifier is the two element set $\Omega 0 = \aset{\ttt,\fff}$:
\[\begin{tikzpicture}[>=stealth]
  \matrix (m) [matrix of math nodes,row sep=4em,column sep=7em,minimum width=2em]
          {
            |(s0)| S0 = \aset{a,b} & |(t)| \aset{0} \\
            |(x0)| X0 = \aset{a,b,\textcolor{red}{c},\textcolor{red}{d}} & |(o)| \aset{\ttt,\textcolor{red}{\fff}} = \Omega 0 \\
          };
          \path[-stealth]
          (s0) edge node [above] {$!$} (t)
          (x0) edge node [above] {$\phi 0$} (o)
          (s0) edge[>->] node [left] {$\alpha 0$} (x0)
          (t) edge[>->] node [right] {$\true$} (o)
          ;
\end{tikzpicture}\]
where $\phi 0 = \{ a \mapsto \ttt, b \mapsto \ttt, \textcolor{red}{c \mapsto \fff}, \textcolor{red}{d \mapsto \fff} \}$.

\bigskip

And same for $S1$ and $X1$, the subobject classifier is $\Omega 1 = \aset{\ttt,\fff}$:

\[\begin{tikzpicture}[>=stealth]
  \matrix (m) [matrix of math nodes,row sep=4em,column sep=7em,minimum width=2em]
          {
            |(s0)| S1 = \aset{a,b,c} & |(t)| \aset{0}\\
            |(x0)| X1 = \aset{a,b,c,\textcolor{red}{d},\textcolor{red}{e}} & |(o)| \aset{\ttt,\textcolor{red}{\fff}} = \Omega 1 \\
          };
          \path[-stealth]
          (s0) edge node [above] {$!$} (t)
          (x0) edge node [above] {$\phi 1$} (o)
          (s0) edge[>->] node [left] {$\alpha 1$} (x0)
          (t) edge[>->] node [right] {$\true$} (o)
          ;
\end{tikzpicture}\]
where $\phi 1 = \{ a \mapsto \ttt, b \mapsto \ttt, c \mapsto \ttt, \textcolor{red}{d \mapsto \fff}, \textcolor{red}{e \mapsto \fff} \}$.

\bigskip

However, if we put these two diagrams together, then it doesn't commute because $\phi 0 (c) = \fff$ while $\phi 1 (c) = \ttt$.
\[\begin{tikzpicture}[>=stealth]
  \matrix (m) [matrix of math nodes,row sep=4em,column sep=4em,minimum width=2em]
          {
            |(s0)| \aset{a,b} &   &  |(t0)| \aset{0} & \\
                              & |(s1)| \aset{a,b,c} & & |(t1)| \aset{0} \\
            |(x0)| \aset{a,b,\textcolor{red}{c},\textcolor{red}{d}} & & |(o0)| \aset{\ttt,\textcolor{red}{\fff}} \\
                              & |(x1)| \aset{a,b,c,\textcolor{red}{d},\textcolor{red}{e}} & & |(o1)| \aset{\ttt, \textcolor{red}{\fff}}\\
          };
          \path[-stealth]
          (s0) edge node [above] {$Sf$} (s1)
          (x0) edge node [above] {$Xf$} (x1)
          (s0) edge[>->] node [near end,left] {$\alpha 0$} (x0)
          (s1) edge[>->,blue] node [near end, left] {$\alpha 1$} (x1)
          (t0) edge node [above] {$!$} (t1)
          (o0) edge node [above] {$\mbox{id}$} (o1)
          (t0) edge[>->] node [near end, right] {$\true$} (o0)
          (t1) edge[>->,blue] node [right] {$\true$} (o1)
          (s0) edge node [near end,above] {$!$} (t0)
          (s1) edge[blue] node [near end, above] {$!$} (t1)
          (x0) edge node [near end, above] {$\phi 0$} (o0)
          (x1) edge[blue] node [near end, above] {$\phi 1$} (o1)
          ;
    \begin{pgfonlayer}{background}
        \node [object, fit=(s0) (s1)] {};
        \node [object, fit=(x0) (x1)] {};
        \node [object, fit=(t0) (t1)] {};
        \node [object, fit=(o0) (o1)] {};
    \end{pgfonlayer}
\end{tikzpicture}\]
We cannot change $\phi 0$, since if $\phi 0(c)=\ttt$, then it would not be a characteristic function for $S0$ in $X0$.

So, the solution is to add an extra element to $\Omega 0 = \aset{\ttt, \uuu, \fff}$ 
and define
\[\phi 0 (c) = \uuu\]
and
\[\Omega f : \Omega 0 \rightarrow \Omega 1 = \{ \ttt \mapsto \ttt, \fff \mapsto \fff, \uuu \mapsto \ttt \}\]

\[\begin{tikzpicture}[>=stealth]
  \matrix (m) [matrix of math nodes,row sep=4em,column sep=4em,minimum width=2em]
          {
            |(s0)| \aset{a,b} &   &  |(t0)| \aset{0} & \\
                              & |(s1)| \aset{a,b,c} & & |(t1)| \aset{0} \\
            |(x0)| \aset{a,b,\textcolor{blue}{c},\textcolor{red}{d}} & & |(o0)| \aset{\ttt,\textcolor{blue}{\uuu},\textcolor{red}{\fff}} \\
                              & |(x1)| \aset{a,b,c,\textcolor{red}{d},\textcolor{red}{e}} & & |(o1)| \aset{\ttt,\textcolor{red}{\fff}}\\
          };
          \path[-stealth]
          (s0) edge node [above] {$Sf$} (s1)
          (x0) edge node [above] {$Xf$} (x1)
          (s0) edge[>->] node [left] {$\alpha 0$} (x0)
          (s1) edge[>->,blue] node [near end, left] {$\alpha 1$} (x1)
          (t0) edge node [above] {$!$} (t1)
          (o0) edge node [above] {$\Omega f$} (o1)
          (t0) edge[>->] node [near end, right] {$\true$} (o0)
          (t1) edge[>->,blue] node [right] {$\true$} (o1)
          (s0) edge node [above] {$!$} (t0)
          (s1) edge[blue] node [near end, above] {$!$} (t1)
          (x0) edge node [near end, above] {$\phi 0$} (o0)
          (x1) edge[blue] node [near end, above] {$\phi 1$} (o1)
          ;
    \begin{pgfonlayer}{background}
        \node [object, fit=(s0) (s1)] (s) {};
        \node [object, fit=(x0) (x1)] {};
        \node [object, fit=(t0) (t1)] {};
        \node [object, fit=(o0) (o1)] {};
    \end{pgfonlayer}
\end{tikzpicture}\]
Then the bottom square commutes:
\[ (\phi 1 \circ Xf) (c) = \ttt = (\Omega f \circ \phi 0) (c) \]
So, the subobject classifier is the functor $\Omega : \arr \rightarrow \sets$ where
\begin{eqnarray*}
 \Omega 0 &=& \aset{\ttt,\uuu,\fff} \\
 \Omega 1 &=& \aset{\ttt ,\fff} \\
 \Omega f : \Omega 0 \rightarrow \Omega 1 &=& \{ \ttt \mapsto \ttt, \fff \mapsto \fff, \uuu \mapsto \ttt \}
\end{eqnarray*}

\end{document}
